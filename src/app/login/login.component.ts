import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  public model: FormGroup;

  constructor(
    public formBuilder: FormBuilder
  ) {
    this.model = this.formBuilder.group({
      userName: ['', [Validators.required, Validators.minLength(3)]],
      password: ['', [Validators.required]]
    });
  }

  public doLogin() {
    const _self = this;
    if(_self.model.valid) {
      console.info('SUCC VALIDATION')
    }
    else{
      let _e_msg = '', _e_count = 0;
      if(_e_count === 0 && !_self.model.controls.userName.valid){
        _e_msg = 'Please enter your valid Name';
        _e_count++;
      }
      if(_e_count === 0 && !_self.model.controls.password.valid){
        _e_msg = 'Please enter your password.';
        _e_count++;
      }
      console.log("ERR", _e_msg);
    }
  }

  ngOnInit() {
    console.log('INIT')
  }

  ngOnDestroy() {
    console.log('DESTROY')
  }
}
